from rusocsci import buttonbox
from helper_functions import *
import my
from datetime import datetime
from psychopy.iohub import launchHubServer
from psychopy.tools.monitorunittools import deg2pix, pix2deg
from psychopy import visual, core

# set the monitor name 
# monitor name should match the name in the monitor center in PsychoPy
monitor = monitors.Monitor('testMonitor')

# initialize a psychopy window, light grey background
win = visual.Window(
	size=(1920, 1080),  # Resolution of the screen
    screen=1,
	units='pix',
	fullscr=True,
	allowGUI=False,
	colorSpace='rgb',
    #monitor.getSizePix(),
	#monitor=monitor,
	color=[0,0,0]
	)

# make cursor invisible
win.mouseVisible = False