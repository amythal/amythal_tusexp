#!/usr/bin/env python

######################################################################################################################################

# import functions
from psychopy import prefs
prefs.general['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = 'ptb'

from rusocsci import buttonbox

import csv, random
import my # import my own functions
import psychtoolbox as ptb
import serial
import sys
import time
import config as psychopy_config
import os


from datetime import datetime
from psychopy import core, clock, visual, event, sound, data
from psychopy.iohub import launchHubServer
from psychopy.iohub.util import hideWindow, showWindow
from psychopy.tools.monitorunittools import deg2pix, pix2deg
#from playsound import playsound

TUS = 1

######################################################################################################################################

if TUS==1:
    from fus_driving_systems.config.config import config_info as config
    from fus_driving_systems.config.logging_config import initialize_logger
    
    log_dir = 'D:\\Users\\amythal\\amythal_tus-exp\\logs'
    filename = "standalone_igt"
    logger = initialize_logger(log_dir, filename)

######################################################################################################################################

timer = 80000000 # us (= 80000 ms)

######################################################################################################################################

# set up BITSI connection

serJr = serial.Serial()
serJr.baudrate = 115200
serJr.port = 'COM1'
serJr.bytesize = 8
parity = 'N'
serJr.stopbits = 1
serJr.timeout = 1
serJr.open()

######################################################################################################################################
if TUS==1:
    
    ##############################################################################
    # first sequence collection
    ##############################################################################
    from sequences import sequence_1_10_ch

    seq1, seq2 = sequence_1_10_ch.create_sequence_collection(logger)
    ##############################################################################
    # second sequence collection
    ##############################################################################
    from sequences import sequence_17_26_ch

    seq3, seq4 = sequence_17_26_ch.create_sequence_collection(logger)

    total_duration_ms = 80000  # [ms]
    
    ##############################################################################
    # connect with driving system and execute sequence
    ##############################################################################
    
    # creating an IGT driving system instance, connecting to it and sending your first sequence can be
    # done when initializing your experiment. When appropriate, execute your sequence by implementing
    # 'execute_sequence()' into your code or by using the external trigger.
    
    # when you want to change your sequence in the middle of your experimental code, create a new
    # sequence as above and send the new sequence: 'send_sequence()'. When appropriate, execute your
    # sequence by implementing 'execute_sequence()' into your code or by using the external trigger.
    
    ##############################################################################
    # import the 'fus_driving_systems - ds' into your code
    ##############################################################################
    
    from fus_driving_systems.igt import igt_ds
    
    igt_driving_sys = igt_ds.IGT(log_dir)

######################################################################################################################################

# setup presentation

# if False: cannot record key presses during core.wait periods
core.checkPygletDuringWait = False

# initialize the participant instructions to be shown at the start of experiment
startInstructions = visual.TextStim(
	win=psychopy_config.win,
	name='startInstructions',
    text='We are about to start the first stimulation. Please fixate the central cross.',
    pos=(0, 0),
    height=0.8,
    wrapWidth=None,
    ori=0,
    color='white',
    colorSpace='rgb',
    opacity=1,
    languageStyle='LTR',
    depth=0.0
    )

# initiliaze fixation cross
fixation = visual.ShapeStim(
	win=psychopy_config.win,
    vertices=((0, -10), (0, 10), (0,0), (-10,0), (10, 0)),
    lineWidth=2,
    closeShape=False,
    lineColor='white'
    ) 

# initiliaze reinstatement fixation circle
fixation_reinstatement = visual.TextStim(
	win=psychopy_config.win,
	text='O',
    pos=(0, 0),
    height=50,
    wrapWidth=None,
    ori=0,
    color='white',
    colorSpace='rgb',
    opacity=1,
    languageStyle='LTR',
    depth=0.0
    )


######################################################################################################################################

ppn = my.getString(psychopy_config.win, "Please enter a participant number:")
# Read in input csv with trial structure, file name should be 'sub-XXX' where XXX are three integers

if 'escape' in ppn:
    print("ESC pressed, closing the script.")
    psychopy_config.win.close()
    core.quit()

if 'dummy' in ppn:
    print("dummy mode active: no TUS communication attempted!!!")
    TUS = 2

# create data log file
# in this file the timing and stimulus types are saved, this file can serve as a sanity check
# to see whether it matches the input .csv file
datafile = my.openDataFile(ppn)

# Connect it with a csv writer
writer = csv.writer(datafile, delimiter=",")

# Create output file header
writer.writerow([
	"ultrasoundOnset", # from clock
	])

######################################################################################################################################

#### IMPORT AUDITORY MASK ####
maskSound = sound.Sound('auditory_matching_stimulus\\prf5Hz_sine16kHz_pulsePattern_GaussianWhiteNoiseSNR14_duration80s_sample44p1kHz.wav', preBuffer = -1)

#### SET UP SEQUENCES ####
if TUS==1:
    igt_driving_sys.connect(seq1.driving_sys.connect_info, log_dir, filename)
    # set up all sequences
    # seq1 - on
    # seq2 - off (=pressure=0)
    # seq3 - off (=pressure=0)
    # seq4 - on
    #igt_driving_sys.send_sequence(seq1, seq2, seq3, seq4, total_duration_ms)
    # or even better wait for trigger
    igt_driving_sys.wait_for_trigger(seq1, seq2, seq3, seq4, total_duration_ms)

######################################################################################################################################

#### START EXPERIMENT ####
        
# record ultrasound transducer positions
key = my.getCharacter(psychopy_config.win, "We will start with the first stimulation block. Please relax and fixate the central cross.")

if 'escape' in key:
    print("ESC pressed, closing the script.")
    psychopy_config.win.close()
    core.quit()

######################################################################################################################################

	
######################################################################################################################################

#### FIXATION CROSS ONSET ####

# show fixation
fixation.draw() # TTT for this, 3 ms need to be substracted from the ITI at the end, to give the draw here three milliseconds
psychopy_config.win.flip()

# save current time as fixation time, at the end of this trial this is written to .dat file (.dat file currently not used for analyses)
fixationTime = clock.getTime()

######################################################################################################################################

if TUS==1:
    try:
        # check thatIGT system is still connected
        #print(igt_ds.is_connected())
        
        #### AUDITORY MASK ONSET ####
        maskSound.play() # sound plays as background process, script can continue during mask playing, before time actually starts is around 9 ms according to Pascal 
        time.sleep(0.009)

        # save tusTime (written to .dat file at end of trial)
        tusTime = clock.getTime()
        
        # Stimulation onset (send starting trigger to execute sequence)
        #binary = '00000100'
        binary = '00100000' # 32
        decimal_number = int(binary, 2)
        print(decimal_number)
        byte_value = bytes([decimal_number])
        print(byte_value)
        serJr.write(byte_value)

        # this alternative could be used to execute the sequence if it has been set up
        # but this will prevent execution of the rest of the script
        #igt_driving_sys.execute_sequence(seq1, seq2, seq3, seq4, total_duration_ms)
        
        n_triggers = round(total_duration_ms / 1000)
        print(n_triggers)
        for i in range(n_triggers):
            # send trigger to EEG & localite
            binary = '01000000' # 64
            decimal_number = int(binary, 2)
            print(decimal_number)
            byte_value = bytes([decimal_number])
            print(byte_value)
            serJr.write(byte_value)
            time.sleep(1)
                
    finally:
        # When the sequence is executed using execute_sequence(), the system will be disconnected
        # automatically. In the case your code is stopped abruptly, the driving system will be
        # disconnected. Otherwise, there is a change that it keeps on firing ultrasound sequences.
        # When using the external trigger, disconnect the driving system yourself.
        if not seq1.wait_for_trigger:
            igt_driving_sys.disconnect()
        # write output to .dat file
        writer.writerow([
        	tusTime, # from clock
        	])
                
else:
    #### AUDITORY MASK ONSET ####
    maskSound.play() # sound plays as background process, script can continue during mask playing, before time actually starts is around 9 ms according to Pascal 

    # save tusTime (written to .dat file at end of trial)
    tusTime = clock.getTime()
    
    # Stimulation onset    
    binary = '00000100'
    decimal_number = int(binary, 2)
    print(decimal_number)
    byte_value = bytes([decimal_number])
    print(byte_value)
    serJr.write(byte_value)

    #send marker to neuronav
    t0_TUS = time.time() # T0
    curtime = time.time()
    while curtime-t0_TUS <= (timer/1000000):
        binary = '01000000'
        decimal_number = int(binary, 2)
        print(decimal_number)
        byte_value = bytes([decimal_number])
        print(byte_value)
        serJr.write(byte_value)
        core.wait(1)
        curtime = time.time()

    # write output to .dat file
    writer.writerow([
    	tusTime, # from clock
    	])

######################################################################################################################################

# show goodbye screen
my.getCharacter(psychopy_config.win, "We have finished the stimulation. We will now move to the MRI room.")

datafile.close()

## Closing Section
psychopy_config.win.close()
core.quit()
