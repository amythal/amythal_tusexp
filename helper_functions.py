# This file includes several helper functions to run the experiment
from psychopy import core, visual, monitors, data, gui
import numpy as np
from itertools import islice
from random import shuffle
from numpy.linalg import norm
from psychopy.iohub.client.eyetracker.validation import TargetStim
from psychopy import iohub
from collections import deque
from psychopy.iohub.constants import EventConstants
from psychopy.iohub.util import hideWindow, showWindow
import shutil


def pix2deg2D(pixels, monitor, correctFlat=False):
    """Convert size of pixels tuple to size in degrees for a given Monitor object
    based on pix2deg from saccade_task.tools.monitorunittools
    """
    # get monitor params and raise error if necessary

    scrWidthCm = monitor.getWidth()
    scrSizePix = monitor.getSizePix()
    dist = monitor.getDistance()
    if scrSizePix is None:
        msg = "Monitor %s has no known size in pixels (SEE MONITOR CENTER)"
        raise ValueError(msg % monitor.name)
    if scrWidthCm is None:
        msg = "Monitor %s has no known width in cm (SEE MONITOR CENTER)"
        raise ValueError(msg % monitor.name)
    if dist is None:
        msg = "Monitor %s has no known distance (SEE MONITOR CENTER)"
        raise ValueError(msg % monitor.name)

    cmSize = np.array(pixels) * float(scrWidthCm) / np.array(scrSizePix)
    degSize = np.degrees(np.arctan(cmSize / dist))
    return degSize

def createFace(win, type='smiley', radius=3, pos=[0, 0], units='deg', lineColor='black'):
    # returns a list of shapes corresponding to a face
    if type == 'smiley':
        baseColor = 'green'
    elif type == 'sadley':
        baseColor = 'red'
    else:
        baseColor = 'gray'

    radius = float(radius)
    pos = np.array(pos)
    baseCircle = visual.Circle(win, radius=radius, units=units, pos=pos, fillColor=baseColor, edges=256, lineColor=lineColor)
    leftEye = visual.Circle(win, radius=0.1 * radius, units=units, fillColor='white', lineColor=lineColor,
                            pos=np.array([-1 / 3, 0.25]) * radius + pos,
                            edges=256)
    rightEye = visual.Circle(win, radius=0.1 * radius, units=units, fillColor='white', lineColor=lineColor,
                             pos=np.array([1 / 3, 0.25]) * radius + pos,
                             edges=256)

    mouthRadius = 0.75 * radius

    if type == 'smiley':
        vertices = [(mouthRadius * np.cos(np.deg2rad(angle)) + pos[0],
                     mouthRadius * np.sin(np.deg2rad(angle)) + pos[1]) for angle in
                    range(-90-25, -90+25)]
        mouth = visual.ShapeStim(win, vertices = vertices, units=units, fillColor=False,
                                 lineColor=lineColor, closeShape = False)
    elif type=='sadley':
        mouthPos = np.array(pos)-np.array([0, 2 * mouthRadius])
        vertices = [(mouthRadius * np.cos(np.deg2rad(angle)) + mouthPos[0],
                     mouthRadius * np.sin(np.deg2rad(angle)) + mouthPos[1]) for angle in
                    range(90-25, 90+25)]
        mouth = visual.ShapeStim(win, vertices = vertices, units=units, fillColor=False, lineColor=lineColor, closeShape = False)
    else:
        mouthWidth = np.cos(np.deg2rad(-90+25)) * 2 * mouthRadius
        mouthY = np.sin(np.deg2rad((-90+25)/2)) * mouthRadius

        mouth = visual.Line(win, start = pos + [-mouthWidth/2, mouthY], end = pos + [mouthWidth/2, mouthY], units = units, lineColor=lineColor)
    return [baseCircle, leftEye, rightEye, mouth]

def generateChunkSeq(seq_type, chunkLengths, delay_bins):
    
    subsess_lengths = [60,60,45]
    chunk_length_rep = [4,4,3]
    delay_bin_full_reps = [5,5,5]
    
    sessions = []
    for subsess in range(3):

        trialList = data.createFactorialTrialList({'delay_bin': np.arange(0, len(delay_bins)).tolist(), 'repN': np.arange(0, delay_bin_full_reps[subsess]).tolist()})
        shuffle(trialList)
        sessions.append(trialList)
    
    # how many trials to move from third subssess to first & 2nd: five
    
    sessions[0] = sessions[0]+sessions[2][0:5]
    del sessions[2][0:5]
    sessions[1] = sessions[1]+sessions[2][0:5]
    del sessions[2][0:5]
    
    res = {}
    
    for subsess in range(3):
        chunk_seq = chunkLengths * chunk_length_rep[subsess]
        shuffle(chunk_seq)
        chunkedTrials = unequal_divide(sessions[subsess], chunk_seq, seq_type)
        #print(chunkedTrials)
        res[subsess] = chunkedTrials

    return res


class extExpHandler(data.ExperimentHandler):
    def addManyData(self, dataList):
        # adds data with names assuming that they are defined as global variables
        for varName in dataList:
            self.addData(varName, globals()[varName])

    def addDataDict(self, dataDict):
        # adds data from a dictionary
        for k, v in dataDict.items():
            self.addData(k, v)

def unequal_divide(iterable, chunks, seq_type):
    it = iter(iterable)
    blocks = [{'trials': list(islice(it, c)), 'seq_type': seq_type, 'block_length': c} for c in chunks]
    return blocks


#from smite import SMITE, helpers


class extTrialHandler(data.TrialHandler):
    def addManyData(self, dataList):
        # adds data with names assuming that they are defined as global variables
        for varName in dataList:
            self.addData(varName, globals()[varName])


def smi2psychopy2d(posArr, mon):
    # takes numpy array of positions in smi coordinates (see helpers.smi2psychopy)
    # make sure that the array is 2D
    if posArr.ndim == 1:
        posArr = np.expand_dims(posArr, 0)

    screen_res = mon.getSizePix()
    # print(str(posArr))
    posArr[:, 0] = posArr[:, 0] - screen_res[0] / 2.0
    posArr[:, 1] = (posArr[:, 1] - screen_res[1] / 2.0) * -1
    # print(posArr)
    # return pix2deg2D(posArr, mon)
    return posArr


def estGazeBias(mon, tracker, smoothWindow=100):
    # print('estimating bias')
    data = tracker.peek_buffer_data()
    if len(data) > 0:
        posArr = np.array([[x.leftEye.gazeX, x.leftEye.gazeY] for x in data][0:smoothWindow])
        medianPos = np.median(posArr, 0)
        medianPosDeg = pix2deg2D(smi2psychopy2d(medianPos, mon), mon)
        if norm(medianPosDeg) < 6:
            #print(posArr.shape)
            #print('Gaze position corrected using %i samples with median pos of %s' % 
            #(posArr.shape[0], np.array2string(medianPos)))
            return medianPos
        else: 
            print('Gaze position not corrected, only %i samples are available' % (len(data)))
    else:
        print('Gaze position not corrected, samples are not available')

    return np.array([0, 0])

def medianGazePosDeg(mon, tracker):
    data = tracker.peek_buffer_data()
    if len(data) > 0:
        posArr = np.array([[x.leftEye.gazeX, x.leftEye.gazeY] for x in data])
        medianPos = np.median(posArr, 0)
        medianPosDeg = pix2deg2D(smi2psychopy2d(medianPos, mon), mon)
        return medianPosDeg
    else:
        False

def validate_eyetracker_iohub(win):
    use_unit_type = 'pix'
    use_color_type = 'rgb'

    target_stim = TargetStim(win, radius=0.025, fillcolor=[.5, .5, .5],
                             edgecolor=[-1, -1, -1], edgewidth=2,
                             dotcolor=[1, -1, -1], dotradius=10,
                             units=use_unit_type, colorspace=use_color_type)

    #target_positions = 'FIVE_POINTS'

    # a bit convoluted but for some reason using height as units does not work, hence recompute
    target_positions = [np.array(i)*win.size for i in [ (-.5,-.5), (-.5, .5), (.5,-.5),(.5, .5)]]
    # Create a validation procedure, iohub must already be running with an
    # eye tracker device, or errors will occur.
    validation_proc = iohub.ValidationProcedure(win,
                                                target=target_stim,  # target stim
                                                positions=target_positions,  # string constant or list of points
                                                randomize_positions=True,  # boolean
                                                expand_scale=1.5,  # float
                                                target_duration=1.5,  # float
                                                target_delay=1.0,  # float
                                                enable_position_animation=True,
                                                color_space=use_color_type,
                                                unit_type=use_unit_type,
                                                progress_on_key="",  # str or None
                                                gaze_cursor=(-1.0, 1.0, -1.0),  # None or color value
                                                show_results_screen=True,  # bool
                                                save_results_screen=False,  # bool, only used if show_results_screen == True
                                                )
    validation_proc.run()
    if validation_proc.results:
        results = validation_proc.results
        print("++++ Validation Results ++++")
        #print(results)
        print("Passed:", results['passed'])
        print("failed_pos_count:", results['positions_failed_processing'])
        print("Units:", results['reporting_unit_type'])
        print("min_error:", results['min_error'])
        print("max_error:", results['max_error'])
        print("mean_error:", results['mean_error'])
    else:
        print("Validation Aborted by User.")
        results = None
    return results

class tracker_helper():
    def __init__(self, tracker, eye_tracker_type, eyeBufferSize = 200, dummy_mode = 0):
        self.tracker = tracker
        self.eye_tracker_type = eye_tracker_type
        self.eyeBufferSize = eyeBufferSize
        self.buffer = deque(maxlen = eyeBufferSize)
        self.dummy_mode = dummy_mode
        
    def send_message(self, msg):
        if self.dummy_mode:
            print(msg)
        elif self.eye_tracker_type=='smi':
            self.tracker.send_message(msg)
        else:
            self.tracker.sendMessage(msg)
    def start_recording(self):
        if self.dummy_mode:
            print('Recording started (no, not really)')
        elif self.eye_tracker_type == 'smi':
            self.tracker.start_recording()
            self.tracker.start_buffer(sample_buffer_length=self.eyeBufferSize)
        else:
            self.tracker.setRecordingState(True)
    def stop_recording(self):
        if self.dummy_mode:
            print('Recording stopped (in reality, it haven\'t even started)')
        elif self.eye_tracker_type == 'smi':
            self.tracker.stop_recording()
        else:
            self.tracker.setRecordingState(False)
    def calibrate(self, win, filename = None):
        if self.dummy_mode:
            return
        tracker = self.tracker
        if self.eye_tracker_type == 'smi':
            if filename is not None:
                tracker.save_data(filename) # idf-file saved in default dir on iView comp
            tracker.calibrate(win)
            self.start_recording()
            tracker.start_buffer(sample_buffer_length=self.eyeBufferSize)
            core.wait(0.5)

            print('connection status: {}'.format(tracker.is_connected()))
            self.stop_recording()
        else:
            hideWindow(win)
            if self.eye_tracker_type!='mouse':
                win.setMouseVisible(False)

            # Display calibration window and run calibration.
            result = tracker.runSetupProcedure()
            # result = tracker.runSetupProcedure()
            print("Calibration returned: ", result)
            # Maximize the PsychoPy window if needed

            showWindow(win)
            if self.eye_tracker_type=='mouse':
                win.setMouseVisible(True)
            #validate_eyetracker_iohub(win)

    def close(self, datafileName = None):
        if datafileName is not None:
            self.saveData(datafileName)
        if self.eye_tracker_type == 'smi':
            self.tracker.de_init()
        else:
            self.tracker.setConnectionState(False)
    def saveData(self, datafileName = None):
        if self.dummy_mode:
            return
        if self.eye_tracker_type == 'smi':
            self.tracker.save_data(datafileName + '.idf') # idf-file saved in default dir on iView comp
        else:
            config = self.tracker.getConfiguration()
            self.tracker.setConnectionState(False)
            if self.eye_tracker_type != 'mouse':
                oldname = config['default_native_data_file_name']+'.edf'
                newname = datafileName+'.edf'
                shutil.move(oldname, newname)
            self.tracker.setConnectionState(True)
            core.wait(5)

    def getEvents(self):
        if self.eye_tracker_type == 'smi':
            data = self.tracker.peek_buffer_data()
            return [[x.leftEye.gazeX, x.leftEye.gazeY] for x in data]
        else:
            for e in self.tracker.getEvents(event_type_id=EventConstants.MONOCULAR_EYE_SAMPLE):
                #print(e)
                self.buffer.appendleft(e)
            return [[x.gaze_x, x.gaze_y] for x in self.buffer]

    def getLastGazePos(self,mon, gazePosBias=np.array([0, 0]), smoothWindow=100):
        data = self.getEvents()
        # print('Getting gaze pos')
        if (len(data) > 0):
            #print(data)
            if (len(data) > smoothWindow):
                posArr = np.array(data[0:smoothWindow])
                pos = np.median(posArr, 0)
            else:
                pos = np.array(data[0])
                pos = pos - gazePosBias
            if self.eye_tracker_type == 'smi':
                pos = smi2psychopy2d(pos, mon)
            return pos
        else:
            return None
