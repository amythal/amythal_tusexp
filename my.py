#!/usr/bin/env python
# -*- coding: utf-8 -*-
# My functions.
  
from psychopy import core, visual, event, clock
from psychopy.hardware import keyboard
import csv, os, time
import random
import serial
import sys
import config #XXXJJJ
ser = serial.Serial()
 
## Function section


def tsgWait(tsgWait, win):
	timeWaitStamp = core.getTime() + tsgWait
	while timeWaitStamp > core.getTime():
		tempKeys = event.getKeys()
		if "escape" in tempKeys:
			config.trackerPointer.stop_recording()
			config.trackerPointer.close(config.eye_data_file)
			config.io.quit()
			sys.exit()
		elif "p" in tempKeys:
			config.bb_physio.sendMarker(val=9) # sends (8,1) as pause begin marker to brainvision
			core.wait(0.01)
			config.bb_physio.sendMarker(val=0) # 
			config.trackerPointer.send_message('pause_begin') #send begin pause marker to eyetracker data
			getCharacter(win, "PAUSE")
			fixation = visual.ShapeStim(
				win=config.win,
    			vertices=((0, -10), (0, 10), (0,0), (-10,0), (10, 0)),
    			lineWidth=2,
    			closeShape=False,
    			lineColor='white'
    			) 
			config.bb_physio.sendMarker(val=9) # sends (8,1) as pause end marker to brainvision
			config.trackerPointer.send_message('pause_end') #send pause end marker to eyetracker data
			core.wait(0.01)
			config.bb_physio.sendMarker(val=0) #
			fixation.draw()
			win.flip()
			core.wait(5)
			break


def distractorTask(win, fixation):
	kb = keyboard.Keyboard()
	nrStars = 27 # 120 / (3 + 1.5) = 26.666.. 
	maxStarLength = 2
	FIX_DISTRACTOR_DURATION = 3
	leftOrRight = [visual.TextStim(win, '<<<<', 0, height=50), visual.TextStim(win, '>>>>', 0, height=50)];
	for i in range(0, nrStars):
		distStimulus = random.choice(leftOrRight);
		distStimulus.draw();
		win.flip();
		fixation.draw();
		waitRest = clock.getTime() + maxStarLength + FIX_DISTRACTOR_DURATION
		keys = event.waitKeys(maxStarLength);
		win.flip()
		while clock.getTime() <= waitRest :
			pass

def presentReinstatement(win, stim_fix, bb):
	stim_fix.draw()
	win.flip()
	tsgWait(float(20),win)                                           
	nr_reinstatement_shocks = 3
	for i in range(nr_reinstatement_shocks):
		tsgWait(float(9.9),win)
		bb.sendMarker(val=3) #signal for shock
		core.wait(0.1)
		bb.sendMarker(val=0) #turn off shock
	tsgWait(float(2),win)


def getCharacter(window, question="Press any key to continue"):
	message = visual.TextStim(window, text=question)
	message.draw()
	window.flip()
	c = event.waitKeys()
	if c:
		return c[0]
	else:
		return ''

def getSpace(window, question="Press space to continue"):
	message = visual.TextStim(window, text=question)
	message.draw()
	window.flip()
	c = event.waitKeys()
	if c == 'space':
		return c[0]
	else:
		return ''

	
def getYN(window, question="Y or N"):
	"""Wait for a maximum of two seconds for a y or n key."""
	message = visual.TextStim(window, text=question)
	message.draw()
	window.flip()
	c = event.waitKeys(maxWait=2.0, keyList = ['y', 'n'])
	if c:
		return c[0]
	else:
		return ''

def getString(window, question="Type: text followed by return"):
	string = ""
	while True:
		message = visual.TextStim(window, text=question+"\n"+string)
		message.draw()
		window.flip()
		c = event.waitKeys()
		if c[0] == 'return':
			return string
		else:
			string = string + c[0]
			
def showText(window, inputText="Text"):
	message = visual.TextStim(window, alignHoriz="center", text=inputText)
	message.draw()
	window.flip()

def openDataFile(ppn=0):
	"""open a data file for output with a filename that nicely uses the current date and time"""
	directory= "data"
	if not os.path.isdir(directory):
		os.mkdir(directory)
	try:
		filename="{}/ppn{}_{}.dat".format(directory, ppn, time.strftime('%Y-%m-%dT%H:%M:%S')) # ISO compliant
		datafile = open(filename, 'w', newline = '')
	except Exception as e:
		filename="{}/ppn{}_{}.dat".format(directory, ppn, time.strftime('%Y-%m-%dT%H.%M.%S')) #for MS Windows
		datafile = open(filename, 'w', newline = '')
	return datafile
	
def getStimulusInputFile(fileName):
	"""Return a list of trials. Each trial is a list of values."""
	# prepare a list of rows
	rows = []
	# open the file
	inputFile = open(fileName, 'r')
	# connect a csv file reader to the file
	reader = csv.reader(inputFile, delimiter=',') ### CHANGED
	# discard the first row, containing the column labels
	reader.__next__() 
	# read every row as a list of values and append it to the list of rows
	for row in reader:
		rows.append(row)
	inputFile.close()
	return rows

def getStimulusInputFileDict(fileName):
	"""Return a list of trials. Each trial is a dict."""
	# prepare a list of rows
	rows = []
	# open the file
	inputFile = open(fileName, 'rb')
	# connect a csv dict file reader to the file
	reader = csv.DictReader(inputFile, delimiter=';')
	# read every row as a dict and append it to the list of rows
	for row in reader:
		rows.append(row)
	inputFile.close()
	return rows
	

def debugLog(text):
	tSinceMidnight = clock.getTime()%86400
	tSinceWholeHour = tSinceMidnight % 3600
	minutes = tSinceWholeHour / 60
	hours = tSinceMidnight / 3600
	seconds = tSinceMidnight % 60
	#print("log {:02d}:{:02d}:{:2.3f}: {}".format(int(hours), int(minutes), seconds, text))
	print("log {:02d}:{:02d}:{:f}: {}".format(int(hours), int(minutes), seconds, text))



