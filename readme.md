### Brainstim lab [DCCN] trigger setup and experimental presentation

**Instructions:**

The following will activate the current python environment containing both the driving system software and psychopy running under python 3.10.

**Important:** If ```TUS = 1;``` in ```experiment_amythal```, stimulation WILL start upon execution (if everything is set up properly, of course).

```
D: # change to D-drive
call D:\Users\amythal\amythal_tus-env\Scripts\activate
cd D:\Users\amythal\amythal_tus-exp\
python experiment_amythal.py
```

[x] Default: display presentation on extended screen (participant) only  
[x] Quit the script by responding to any request with the ESC button
[x] Dummy mode: enter "dummy" as participant, then no triggers will be sent to driving system

----

Old version containing no integration of the FUS driving system. The virtual environment ```amythal-env``` is now deprecated as it only includes the python 3.8-based materials to run the presentation.

```
D: # change to D-drive
D:\Users\amythal\amythal-env\Scripts\activate
cd D:\Users\amythal\amythal_tus-exp\
python experiment_amythal.py
```

Note: These scripts have been adapted from ThalStim.

TO DO: make sure the full 80s masking stimulus is running. 

## To set up a virtual environment in the brainstim lab follow these instructions:

```
D:
cd D:\Users\amythal
"C:\Program Files\Python310\python.exe" -m venv D:\Users\amythal\amythal_tus-env
# previously, we used the following
#"C:\python38\python.exe" -m venv amythal-env
amythal-env\Scripts\activate
pip install psychopy 
pip install psychtoolbox pyserial numpy rusocsci
cd D:\Users\amythal\amythal_tus-exp\
"C:\python38\python.exe" experiment_amythal.py
```

**Note:** The following does not work as the FUS package requires python 3.10, whereas psychopy prefers 3.8.

To include the driving system software (using python 3.8 for now), run the following after activating the environment. Note that you may have to re-run this if the driving software needs to be updated.

```
cd D:\Users\julkos\Radboud-FUS-driving-system-software
pip install .\fus_ds_package
pip install -r requirements.txt
```

15287 01001 (seq1 on, seq4 off)
15473 01002 (seq2 off, seq3 on)

**Note:** The other way around may work: activate the 3.10 python environment, then install psychopy et al.; ```amythal_tus-env``` is a clone of the ```FUS_DS_PACKAGE``` hosted in the ```julkos``` folder.

```
D:
call D:\Users\amythal\amythal_tus-env\Scripts\activate
pip install git+https://github.com/psychopy/psychopy --ignore-requires-python
pip install psychtoolbox pyserial numpy rusocsci playsound psychopy-pyo psychopy-sounddevice
```

Once successfully set up, we can run the script at the beginning.